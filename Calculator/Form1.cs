﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Calculator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        // 入力された数字
        string input_str = "";
        // 計算結果
        double result = 0;
        //押された演算子
        string Operator = null;

        private void Num_Clicked(object sender, EventArgs e)
        {
            //senderの情報を扱えるようにするため
            //senderはどのコントロールかの情報
            Button btn = (Button)sender;
            //押されたボタンの数字 
            string text = btn.Text;
            //入力された数字に連結
            input_str += text;
            //画面上に数字を出す
            this.textBox1.Text = input_str;
        }

        private void OperatorClicked(object sender, EventArgs e)
        {
            double num1 = result;
            double num2;
            // 入力された文字が空欄なら、計算をスキップする
            if (input_str != "")
            {
                num2 = double.Parse(input_str);
                //四則演算
                if (Operator == "＋")
                {
                    result = num1 + num2;
                }
                if (Operator == "－")
                {
                    result = num1 - num2;
                }
                if (Operator == "÷")
                {
                    result = num1 / num2;
                }
                if (Operator == "×")
                {
                    result = num1 * num2;
                }
                
                //一回目はまだOperator = btn.Textの処理をしていないためOperatorは初期値null
                //なのでresultに一回目の入力数値を入れる処理をしている
                if (Operator == null)
                {
                   result = num2;
                }
            }
            // 画面に計算結果を表示する
            this.textBox1.Text = result.ToString();

            //毎回初期化しないといけない
            input_str = "";

            //演算子をOperator変数に入れる
            //一回目の演算子を発動させずに一回目入力の値をresultに入れるため
            //このタイミングで処理を入れている　
            Button btn = (Button)sender;
            Operator = btn.Text;

            if (Operator == "＝")
            {
                //二回目でif文をスルーさせてthis.textBox1.Text = result.ToString()
                //につなげるため
                Operator = "";
            }
            //ACの処理
            if (Operator == "AC")
            {
                this.textBox1.Text = "0";
                input_str = "";
                //Operatorも初期化してあげないとOperatorにACが入った状態になり
                //if文がスルーされ、resultに一回目の入力が入らない
                Operator = null;
            }

        }
    }

}

