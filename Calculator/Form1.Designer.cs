﻿namespace Calculator
{
    partial class Form1
    {
        /// <summary>
        /// 必要なデザイナー変数です。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 使用中のリソースをすべてクリーンアップします。
        /// </summary>
        /// <param name="disposing">マネージド リソースを破棄する場合は true を指定し、その他の場合は false を指定します。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows フォーム デザイナーで生成されたコード

        /// <summary>
        /// デザイナー サポートに必要なメソッドです。このメソッドの内容を
        /// コード エディターで変更しないでください。
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button0 = new System.Windows.Forms.Button();
            this.dividedButton = new System.Windows.Forms.Button();
            this.multiplyButton = new System.Windows.Forms.Button();
            this.plusButton = new System.Windows.Forms.Button();
            this.minusButton = new System.Windows.Forms.Button();
            this.equalButton = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.AllClear = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // textBox1
            // 
            this.textBox1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textBox1.Font = new System.Drawing.Font("MS UI Gothic", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(128)));
            this.textBox1.Location = new System.Drawing.Point(60, 22);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(156, 31);
            this.textBox1.TabIndex = 0;
            this.textBox1.Text = "0";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(27, 185);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(51, 36);
            this.button1.TabIndex = 1;
            this.button1.Text = "1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.Num_Clicked);
            // 
            // button2
            // 
            this.button2.Location = new System.Drawing.Point(84, 185);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(50, 36);
            this.button2.TabIndex = 2;
            this.button2.Text = "2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.Num_Clicked);
            // 
            // button3
            // 
            this.button3.Location = new System.Drawing.Point(141, 185);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(53, 36);
            this.button3.TabIndex = 3;
            this.button3.Text = "3";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.Num_Clicked);
            // 
            // button4
            // 
            this.button4.Location = new System.Drawing.Point(27, 135);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(51, 37);
            this.button4.TabIndex = 4;
            this.button4.Text = "4";
            this.button4.UseVisualStyleBackColor = true;
            this.button4.Click += new System.EventHandler(this.Num_Clicked);
            // 
            // button5
            // 
            this.button5.Location = new System.Drawing.Point(84, 135);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(50, 37);
            this.button5.TabIndex = 5;
            this.button5.Text = "5";
            this.button5.UseVisualStyleBackColor = true;
            this.button5.Click += new System.EventHandler(this.Num_Clicked);
            // 
            // button6
            // 
            this.button6.Location = new System.Drawing.Point(141, 135);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(53, 37);
            this.button6.TabIndex = 6;
            this.button6.Text = "6";
            this.button6.UseVisualStyleBackColor = true;
            this.button6.Click += new System.EventHandler(this.Num_Clicked);
            // 
            // button7
            // 
            this.button7.Location = new System.Drawing.Point(27, 84);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(51, 36);
            this.button7.TabIndex = 7;
            this.button7.Text = "7";
            this.button7.UseVisualStyleBackColor = true;
            this.button7.Click += new System.EventHandler(this.Num_Clicked);
            // 
            // button8
            // 
            this.button8.Location = new System.Drawing.Point(84, 84);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(50, 36);
            this.button8.TabIndex = 8;
            this.button8.Text = "8";
            this.button8.UseVisualStyleBackColor = true;
            this.button8.Click += new System.EventHandler(this.Num_Clicked);
            // 
            // button9
            // 
            this.button9.Location = new System.Drawing.Point(141, 84);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(53, 36);
            this.button9.TabIndex = 9;
            this.button9.Text = "9";
            this.button9.UseVisualStyleBackColor = true;
            this.button9.Click += new System.EventHandler(this.Num_Clicked);
            // 
            // button0
            // 
            this.button0.Location = new System.Drawing.Point(27, 227);
            this.button0.Name = "button0";
            this.button0.Size = new System.Drawing.Size(51, 35);
            this.button0.TabIndex = 10;
            this.button0.Text = "0";
            this.button0.UseVisualStyleBackColor = true;
            this.button0.Click += new System.EventHandler(this.Num_Clicked);
            // 
            // dividedButton
            // 
            this.dividedButton.Location = new System.Drawing.Point(200, 84);
            this.dividedButton.Name = "dividedButton";
            this.dividedButton.Size = new System.Drawing.Size(58, 36);
            this.dividedButton.TabIndex = 11;
            this.dividedButton.Text = "÷";
            this.dividedButton.UseVisualStyleBackColor = true;
            this.dividedButton.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // multiplyButton
            // 
            this.multiplyButton.Location = new System.Drawing.Point(200, 135);
            this.multiplyButton.Name = "multiplyButton";
            this.multiplyButton.Size = new System.Drawing.Size(58, 37);
            this.multiplyButton.TabIndex = 12;
            this.multiplyButton.Text = "×";
            this.multiplyButton.UseVisualStyleBackColor = true;
            this.multiplyButton.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // plusButton
            // 
            this.plusButton.Location = new System.Drawing.Point(200, 185);
            this.plusButton.Name = "plusButton";
            this.plusButton.Size = new System.Drawing.Size(58, 36);
            this.plusButton.TabIndex = 13;
            this.plusButton.Text = "＋";
            this.plusButton.UseVisualStyleBackColor = true;
            this.plusButton.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // minusButton
            // 
            this.minusButton.Location = new System.Drawing.Point(200, 228);
            this.minusButton.Name = "minusButton";
            this.minusButton.Size = new System.Drawing.Size(58, 34);
            this.minusButton.TabIndex = 14;
            this.minusButton.Text = "－";
            this.minusButton.UseVisualStyleBackColor = true;
            this.minusButton.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // equalButton
            // 
            this.equalButton.Location = new System.Drawing.Point(141, 228);
            this.equalButton.Name = "equalButton";
            this.equalButton.Size = new System.Drawing.Size(53, 34);
            this.equalButton.TabIndex = 15;
            this.equalButton.Text = "＝";
            this.equalButton.UseVisualStyleBackColor = true;
            this.equalButton.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // button15
            // 
            this.button15.Location = new System.Drawing.Point(84, 228);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(51, 34);
            this.button15.TabIndex = 16;
            this.button15.Text = ".";
            this.button15.UseVisualStyleBackColor = true;
            this.button15.Click += new System.EventHandler(this.Num_Clicked);
            // 
            // button16
            // 
            this.button16.Location = new System.Drawing.Point(84, 84);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(51, 36);
            this.button16.TabIndex = 7;
            this.button16.Text = "7";
            this.button16.UseVisualStyleBackColor = true;
            // 
            // AllClear
            // 
            this.AllClear.Location = new System.Drawing.Point(200, 270);
            this.AllClear.Name = "AllClear";
            this.AllClear.Size = new System.Drawing.Size(58, 23);
            this.AllClear.TabIndex = 17;
            this.AllClear.Text = "AC";
            this.AllClear.UseVisualStyleBackColor = true;
            this.AllClear.Click += new System.EventHandler(this.OperatorClicked);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(289, 305);
            this.Controls.Add(this.AllClear);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.equalButton);
            this.Controls.Add(this.minusButton);
            this.Controls.Add(this.plusButton);
            this.Controls.Add(this.multiplyButton);
            this.Controls.Add(this.dividedButton);
            this.Controls.Add(this.button0);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.textBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Name = "Form1";
            this.Text = "Form1";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Button button4;
        private System.Windows.Forms.Button button5;
        private System.Windows.Forms.Button button6;
        private System.Windows.Forms.Button button7;
        private System.Windows.Forms.Button button8;
        private System.Windows.Forms.Button button9;
        private System.Windows.Forms.Button button0;
        private System.Windows.Forms.Button dividedButton;
        private System.Windows.Forms.Button multiplyButton;
        private System.Windows.Forms.Button plusButton;
        private System.Windows.Forms.Button minusButton;
        private System.Windows.Forms.Button equalButton;
        private System.Windows.Forms.Button button15;
        private System.Windows.Forms.Button button16;
        private System.Windows.Forms.Button AllClear;
    }
}

